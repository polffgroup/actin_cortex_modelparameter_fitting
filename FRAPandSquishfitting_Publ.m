function main()
% This program determines the parameters of the binding dynamics model by
% fitting the average FRAP recovery curves and cell squishing data (time
% evolutions of cortex-to-cytoplasm ratio and cortical tension, averaged).
%
% INPUT: the files "AveragedRecoveryFRAP_ProteinName.fig", and
% "Tension_CortexFluo_Squish__FlnB.fig"
% OUTPUT: the files "Output_FRAPSquish_Fitting.txt" and
% "FittingFRAP_Squish_ProteinName_RandomString.fig"
%
% All time scales are in seconds, all tension values in mN/m, all lengths
% in um.

global CutOff2
path='Yourpath/';
method='LTension'; % linear dependence of lifetime on tension
Pattern='FlnB'; %This sting must also be in the name of both input files.
Pattern2='Int';
files=dir([path   'AveragedRecover*' Pattern '*.fig']);

weight=0.1; % weight for cortical fraction data
weight2=2; % weight for squishing data
CutOffP=0;% cuts off the FRAP recovery in the beginning
CutOff2=0; % cuts off points (1:CutOff2) in the squishing curve from fitting 
%tcomp=5; tcomp2=15; % set further below
lbSurfIncr=0.02; % lower bound for fitted surface increase
ActinRecTS=28; % actin level recovery time scale determined from squishing of Hela with fluroescent Lifeact
ActinTUR=0.; %Adding a finite value ActinTUR (such as 1/60) to the tension dependent ratio koff2, does not give good fits


close all;
delt=0.1; % time step for Euler forward intergration algorithm
V0=5000;  % estimated average cell volume (from meausrements on FRAPed cells for each construct)
h0=15; %estimated average initial cell height before squishing (from measurement on Squished cells for each construct)
delh=5; % height change in um during squishing;

if strcmp(Pattern, 'FlnB')==1
    %FlnB
    tcomp=5; tcomp2=100; %time points on FRAP curve where measued data and fit are coinciding
    weight2=2;% weight for squishing data
    SquishTotTime=310;
    sigm1=2.2% 1.82; % mean-field cortical tension in control conditions
    sigm2=0.48%0.38; % mean-field cortical tension in tension-reduced conditions
    F1=0.27%0.24; %cortical fraction in control condition (median value)
    F2=0.20%0.19; %cortical fraction in tension-reduced condition (median value)
    RecTime=8E3; % estimated time scale in seconds for the bleaching correction of the cell squishing intensity ratio curves
    resc=1;%rescaling of cortical concentration due to surface area change
    V0=4400; h0=16.1;delh=5;
elseif strcmp(Pattern, 'FlnA')==1
    %FlnA
    tcomp=5; tcomp2=45; weight2=2;
    SquishTotTime=260;
    sigm1=1.74; % mean-field cortical tension in control conditions
    sigm2=0.33; % mean-field cortical tension in tension-reduced conditions
    F1=0.40; %cortical fraction in control condition (median value)
    F2=0.26; %cortical fraction in tension-reduced condition (median value)
    RecTime=3.7E3; 
    resc=0.9/0.8/1.125;
    V0=4100; h0=14.3;delh=5;
elseif strcmp(Pattern, 'Actn1')==1
    %Actn1
    tcomp=5; tcomp2=20; weight2=2;
    SquishTotTime=310;
    sigm1=2.93; % mean-field cortical tension in control conditions
    sigm2=0.45; % mean-field cortical tension in tension-reduced conditions
    F1=0.20; %cortical fraction in control condition (median value)
    F2=0.16; %cortical fraction in tension-reduced condition (median value)
    RecTime=9.5E3; 
    resc=1;
    V0=4700; h0=14.3;delh=3;
elseif strcmp(Pattern, 'Myh9')==1
    %Myh9
    tcomp=20; tcomp2=100; weight2=2;
    SquishTotTime=320;
    sigm1=1.67; % mean-field cortical tension in control conditions
    sigm2=0.61; % mean-field cortical tension in tension-reduced conditions
    F1=0.23; %cortical fraction in control condition (median value)
    F2=0.23; %cortical fraction in tension-reduced condition (median value)
    RecTime=2.2E4; 
    resc=1;
    V0=4900; h0=13.8;delh=3.4;
else
    %Actn4
    tcomp=10; tcomp2=20; weight2=2; 
    lbSurfIncr=0.07;
    SquishTotTime=200;
    sigm1=2.9; % mean-field cortical tension in control conditions
    sigm2=0.45; % mean-field cortical tension in tension-reduced conditions
    F1=0.2; %cortical fraction in control condition (median value)
    F2=0.155; %cortical fraction in tension-reduced condition (median value)
    RecTime=850000;
    resc=1.005;
end
    
FileName=files(1).name;
fig=openfig([path, FileName]);
axesObjs = get(fig, 'Children');
dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
ydata = get(dataObjs, 'YData');
timesFRAP=xdata{2};
CtrlRec=ydata{6}; 
CondRec=ydata{2}; 

files=dir([path   'Tension_CortexFluo_Squish_*' Pattern '*.fig']);
fig=openfig([path, files(1).name]);
axesObjs = get(fig, 'Children');
dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
ydata = get(dataObjs, 'YData');
timesSquish=xdata{1}; timesSquish=timesSquish(1:SquishTotTime);
Tension=ydata{2}; Tension=Tension(1:SquishTotTime);
CorticalFluo=interp1(xdata{5},ydata{5},xdata{5}(1):xdata{5}(end)); %interpolation to timestep of 1 second
CorticalFluo=CorticalFluo(1:SquishTotTime).*exp(-timesSquish/RecTime);
CorticalFluo=CorticalFluo/CorticalFluo(SquishTotTime);

CorticalFluo(isnan(CorticalFluo))=CorticalFluo(min(find(isfinite(CorticalFluo)))); %set values at negative times equal to time at t=0
Surfs=AreaCalc(timesSquish, V0, h0, delh);
CorticalFluo=CorticalFluo.*Surfs(end)./Surfs*resc; %rescaling of cortical concentration due to surface area change, concerns only time steps before cantilever comes to a halt
SurfToVol=Surfs(1)/V0; R=3/SurfToVol;

%para0=[(0.065) 0.01 1/30 10 (CtrlRec(end)-CtrlRec(10)) (CondRec(end)-CondRec(10)) CtrlRec(10) CondRec(10)]; % guessed parameters for reaction dynamics
%para0=[(0.065) 0.01 1/30 2 (CtrlRec(end)-CtrlRec(3)) (CondRec(end)-CondRec(3)) ]; % guessed parameters for reaction dynamics
para0=[(0.007) 0.01 1/10 1 0.15];

%[C,I]=max(Tension);
I=11; 
if strcmp(Pattern, 'Myh9')==1; I=21; end; %For Myh9 only
newopts=optimset('lsqcurvefit');
optnew = optimset(newopts,'TolX',1e-18, 'MaxFunEvals',2800);
[para,resnorm,residual,exitflag,output,lambda,jacobian] =lsqnonlin(@CalcMisMatch , para0, [0 0.00 0 0 lbSurfIncr ],[10 100 5 1000 0.2 ], optnew,...
    timesFRAP, CtrlRec, CondRec,timesSquish , Tension,  SurfToVol, delt,F1, F2, sigm1, sigm2);

temp=nlparci(para,residual,'jacobian',jacobian);
delpara2(:)=(temp(:,2)-temp(:,1))/2;

para
kon1=para(1); kon2=para(2); koff=para(3); alpha=para(4); 
%first: high tension case
%koff2=koff./(1+sigm1*alpha);
koff2=(ActinTUR+koff./(1+sigm1*alpha));
Ratio1=(3*kon1*kon2)/(3*kon1*kon2+6*koff2*kon1); % ratio of cross-linking homodimers at the cortex
Ratio1=Surfs(end)*(kon1*kon2)/(2*koff2(1)*kon1*Surfs(end)+kon2*kon1*Surfs(end) + koff*koff2(1)*V0); % ratio of cross-linking homodimers in total number of molecules

%second: low tension case
koff2=(ActinTUR+koff./(1+sigm2*alpha));
Ratio2=(3*kon1*kon2)/(3*kon1*kon2+6*koff2*kon1); % ratio of cross-linking homodimers at the cortex
Ratio2=Surfs(end)*(kon1*kon2)/(2*koff2(1)*kon1*Surfs(end)+kon2*kon1*Surfs(end) + koff*koff2(1)*V0); % ratio of cross-linking homodimers in total number of molecules

% least square fit of binding dynamics model with measured data
dist=CalcMisMatch(para, timesFRAP, CtrlRec, CondRec, timesSquish, Tension, SurfToVol, delt,F1, F2, sigm1, sigm2);

figure(5); subplot(2,1,1); hold off;
plot(timesFRAP(CutOffP+1:end), -dist(3:(2+length(timesFRAP)-CutOffP))+CtrlRec(CutOffP+1:end),'b-','LineWidth',2);
hold on; plot(timesFRAP, CtrlRec,'bo');
plot(timesFRAP(CutOffP+1:end), -dist((3+length(timesFRAP)-CutOffP):(2+2*length(timesFRAP)-2*CutOffP))+CondRec(CutOffP+1:end),'r-','LineWidth',2);
hold on; plot(timesFRAP, CondRec,'ro');
title([Pattern ' FRAP '])

subplot(2,1,2); hold off;
plot(timesSquish(I+CutOff2:end), dist(((2+2*length(timesFRAP)-2*CutOffP)+1):end)/weight2+CorticalFluo(I+CutOff2:end),'b-');
hold on; plot(timesSquish, CorticalFluo,'mo');
['fitted param.: ' num2str([1/kdetF(para, sigm1), 1/kdetF(para, sigm2)])]
title( {[method Pattern ' squishing, BLEACHING CORRECTED '],...
    ['Fitted Cortical fractions: ',num2str([CorticalFraction(para, sigm1), CorticalFraction(para, sigm2)])] ,...
    ['Fitted time scale: ', num2str([1/kdetF(para, sigm1), 1/kdetF(para, sigm2)])] ,...
    ['N_{cl}: ', num2str([Ratio1, Ratio2])] ,...
    ['Fitted Param.: kon1=' num2str(para(1),'%.4f') ', kon2=' num2str(para(2),'%.4f')],...
    [ ' koff(0)=' num2str(para(3),'%.3f') ', alpha=' num2str(para(4),'%.3f'), ' RelSurfIncr=' num2str(para(5),'%.3f')]...
    ,['Errors: ' num2str(delpara2)]})


saveas(gcf,[path, 'Fitting_FRAP_Squish_' Pattern '_B_' Pattern2 'weighted.fig'])
%title(num2str([CorticalFraction(para, sigm1), CorticalFraction(para, sigm2)]))

% Save results in text file:[
    file_id = fopen([path 'Output_FRAPSquish_Fitting.txt'],'a');    
    fprintf(file_id,'%s \t',[method Pattern]);
    fprintf(file_id,'%g \t',para, Ratio1, Ratio2, weight, weight2, CutOffP, CutOff2, tcomp, ...
        tcomp2, lbSurfIncr, ActinTUR, ActinRecTS, V0,h0,delh, delt, SquishTotTime,sigm1,sigm2,F1,F2,RecTime);
    fprintf(file_id,'\n');
    fclose(file_id);
    
    


function dist=CalcMisMatch(para, timesFRAP, FRAPrec1, FRAPrec2, timesSquish, Tension, SurfToVol,delt, F1, F2, sigm1, sigm2)
        %distance function for the least square fitting of the binding
        %dynamics model with measured data
        
        kon1=para(1); kon2=para(2); koff=para(3); alpha=para(4); 
        offset1=FRAPrec1(3); offset2=FRAPrec2(3);
        %offset1=para(7); offset2=para(8);
        %Ntot1=1-offset1; Ntot2=1-offset2;
        %Ntot1=para(5); Ntot2=para(6);
        tinterval=timesFRAP(2)-timesFRAP(1);
        
        dist=zeros(1,2*length(timesFRAP)+2-CutOffP*2);
        t=timesFRAP(1:length(timesFRAP));
        R=3/SurfToVol;
        
        %first: high tension case
        koff2=(ActinTUR+koff./(1+sigm1*alpha));
        kdet=-(1/2)*(-koff-2*koff2-kon2+sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
        kdetpr=-(1/2)*(-koff-2*koff2-kon2-sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
        kt=kdetpr-kdet;%sqrt(koff^2+(2*koff2+kon2)^2+koff*(-4*koff2+2*kon2));
        Frac=(3*kon1*(2*koff2+kon2))/(koff*koff2*R+3*kon1*(2*koff2+kon2));
        
        dist(1,1)=abs((Frac-F1))*length(timesFRAP)*weight;
        %RecInt=(((-exp(-kdet*t)+exp(-kdetpr*t))*koff*(2*koff2 -kon2)-(2*koff2+kon2)*...
        %    (2*(-exp(-kdet*t)+exp(-kdetpr*t))*koff2+(-exp(-kdet*t)+exp(-kdetpr*t))*kon2...
        %    +(exp(-kdet*t)-2+exp(-kdetpr*t))*kt)))/(2*kt*(2*koff2+kon2));
        RecInt=kon1*((exp(-kdetpr*t)-exp(-kdet*t))*(koff*(kon2 -2*koff2)+(2*koff2+kon2)^2)...
               -kt*(exp(-kdetpr*t)+exp(-kdet*t)-2)*(2*koff2+kon2))...
               /(2*kt*(4*pi*R^2)*(2*koff2*kon1+kon1*kon2+koff*koff2*R/3));
        A=(FRAPrec1(tcomp2/tinterval)-FRAPrec1(tcomp/tinterval))/(RecInt(tcomp2/tinterval)-RecInt(tcomp/tinterval));
        B=FRAPrec1(tcomp2/tinterval)-A*RecInt(tcomp2/tinterval);
        dist(1,3:(2+length(timesFRAP)-CutOffP))=(FRAPrec1(CutOffP+1:end)-(A*RecInt(CutOffP+1:end)+B));
        
        %second: low tension case
        koff2=(ActinTUR+koff./(1+sigm2*alpha));
        kdet=-(1/2)*(-koff-2*koff2-kon2+sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
        kdetpr=-(1/2)*(-koff-2*koff2-kon2-sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
        kt=kdetpr-kdet;
        Frac=(3*kon1*(2*koff2+kon2))/(koff*koff2*R+3*kon1*(2*koff2+kon2));
        
        
        dist(1,2)=abs(Frac-F2)*length(timesFRAP)*weight;
        
        RecInt=kon1*((exp(-kdetpr*t)-exp(-kdet*t))*(koff*(kon2 -2*koff2)+(2*koff2+kon2)^2)...
               -kt*(exp(-kdetpr*t)+exp(-kdet*t)-2)*(2*koff2+kon2))...
               /(2*kt*(4*pi*R^2)*(2*koff2*kon1+kon1*kon2+koff*koff2*R/3));
        A=(FRAPrec2(tcomp2/tinterval)-FRAPrec2(tcomp/tinterval))/(RecInt(tcomp2/tinterval)-RecInt(tcomp/tinterval));
        B=FRAPrec2(tcomp2/tinterval)-A*RecInt(tcomp2/tinterval);
        dist(1,(3+length(timesFRAP)-CutOffP):end)=(FRAPrec2(CutOffP+1:end)-(A*RecInt(CutOffP+1:end)+B));
        
        dist2=CalcMisMatch2(para, timesSquish, Tension, CorticalFluo, Surfs,delt,  F1, F2, sigm1, sigm2,I);
        dist=[dist (weight2*dist2(CutOff2+1:end))];
        
        function dist2=CalcMisMatch2(para, times, Tension, CorticalFluo, Surfs,delt,  F1, F2, sigm1, sigm2,I)
            RelSurfIncr=max(Surfs/Surfs(1)-1);
            Surfs=((Surfs/Surfs(1)-1)/RelSurfIncr*para(5)+1)*Surfs(1);
            Surfs=interp1(times, Surfs, times(1):delt:times(end));
            kon1=para(1); kon2=para(2); koff=para(3); alpha=para(4);   Surf0=Surfs(1); Vol=5000;
            Surfend=Surfs(end); % surface area in the beginning and in the end;
            
            R=Vol/Surf0*3;
            ind=round((I-1)/delt)+1;
            times2=times(1):0.1:times(end);
            
            koff2high=(ActinTUR+koff./(1+sigm1*alpha)); koff2low=(ActinTUR+koff./(1+sigm2*alpha));
            Frac1=(3*kon1*(2*koff2high+kon2))/(koff*koff2high*R + 3*kon1*(2*koff2high + kon2));
            Frac2=(3*kon1*(2*koff2low+kon2))/(koff*koff2low*R + 3*kon1*(2*koff2low + kon2));
            dist2=zeros(1,length(Tension)-10);
            if strcmp(Pattern, 'Myh9')==1; dist2=zeros(1,length(Tension)-20); end;%For Myh9 only
            
            cmem1=zeros(1,length(Surfs));
            cmem2=zeros(1,length(Surfs));
            
            koff2=(ActinTUR+koff./(1+interp1(times,Tension, (times(1):delt:times(end)))*alpha)); %detachment rate koff2 in dependence of time
            kdet=-(1/2)*(-koff-2*koff2-kon2+sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
            
            
            cmem10=(2*koff2(1)*kon1)./(2*koff2(1)*kon1*Surf0+kon1*kon2*Surf0+koff*koff2(1)*Vol); % steady state concentration before squishing
            cmem20=(kon1*kon2)/(2*koff2(1)*kon1*Surf0+kon2*kon1*Surf0 + koff*koff2(1)*Vol); % steady state concentration before squishing
            cmem1end=(2*koff2(end)*kon1)./(2*koff2(end)*kon1*Surfend+kon1*kon2*Surfend+koff*koff2(end)*Vol);% steady state concentration after squishing
            cmem2end=(kon1*kon2)/(2*koff2(end)*kon1*Surfend+kon2*kon1*Surfend + koff*koff2(end)*Vol);% steady state concentration after squishing
            cmem1(1)=cmem10; cmem2(1)=cmem20;
            for i=1:(length(cmem1)-1)
                try
                    cmem1(i+1)=delt*(2*kon1*(Surfs(1)./Surfs(i+1).*(1+para(5)*heaviside(times2(i+1)).*(1-exp(-times2(i+1)/ActinRecTS))))...
                                            *(1-(cmem1(i) + cmem2(i))*Surfs(i+1))/Vol - ...
                                             koff*cmem1(i)+ 2*koff2(i)*cmem2(i) - kon2*cmem1(i))+cmem1(i);
                    cmem1(i+1)=cmem1(i+1)*(Surfs(i))/Surfs(i+1); % take into account change in concentration due to area increase
                    cmem2(i+1)=delt*(kon2*cmem1(i)-2*koff2(i)*cmem2(i))+cmem2(i);%-cmem2(i)*(Surfs(i+1)-Surfs(i))/Surfs(i)
                    cmem2(i+1)=cmem2(i+1)*(Surfs(i))/Surfs(i+1);
                catch
                end
            end
            try
                dist2(:)=((cmem1(ind:round(1/delt):end)...
                    +cmem2((ind:round(1/delt):end)))./((1-(cmem1((ind:round(1/delt):end))...
                    + cmem2((ind:round(1/delt):end)))*Surfend)/Vol)...
                    /((cmem1end+cmem2end)/((1-(cmem1end + cmem2end)*Surfend)/Vol))-CorticalFluo(I:end));
            catch
            end
            1;
            
            function result=kdetF(gamma)
                koff2=(ActinTUR+koff./(1+gamma*alpha));
                result=-(1/2)*(-koff-2*koff2-kon2+sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
            end
        end
    end

    function result=CorticalFraction(para, gamma)
        kon1=para(1); kon2=para(2); koff=para(3); alpha=para(4);
        koff2=(ActinTUR+koff./(1+gamma*alpha));
        result=(3*kon1*(2*koff2+kon2))/(koff*koff2*R + 3*kon1*(2*koff2 + kon2));
    end
    function result=kdetF(para, gamma)
        kon1=para(1); kon2=para(2); koff=para(3); alpha=para(4);
        koff2=(ActinTUR+koff./(1+gamma*alpha));
        result=-(1/2)*(-koff-2*koff2-kon2+sqrt(-8*koff*koff2+(koff+2*koff2+kon2).^2));
    end
    function Surfs=AreaCalc(times, V0, h0, delh)
        % outputs the surface area of the cell during squishing for every
        % second
        TS=times(2)-times(1);
        R0=(3/4*V0/pi)^0.333;
        hs= 0*times+h0-delh; hs(1:round(delh/(0.5*TS)))=h0-0.5*(times(1:round(delh/(0.5*TS)))-times(1));
        if strcmp(Pattern, 'Myh9')==1
            hs(4:20)=h0-0.2*(times(4:20)-times(4));%For Myh9 only
            hs(1:3)=h0;%For Myh9 only
        end
        if strcmp(Pattern, 'Actn1')==1;
            hs(5:10)=h0-0.5*(times(5:10)-times(5));%For Actn1 only
            hs(1:4)=h0;%For Actn1 only
        end
        R2s=R2calc(hs, V0+0*hs);
        Surfs=Area(R2s, hs/2, hs);
        
        function Vol=Volume(R2,Z, R1)
            if Z>(2*R2)
                Z=2*R2; R1=R2;
            end
            Vol=pi*(Z.*(2*R1.^2 -2*R1.*R2 + R2.^2-Z.^2/12)+(2*asin(0.5*Z./R1)+sin(2*asin(0.5*Z./R1))).*(R2 - R1).*R1.^2);
        end
        
        function R2=R2calc(Z, V0)
            Rs=0*Z;
            start=12;
            for i=1:length(Z)
                if Z(i)>(2*R0)
                    R2(i)=R0;
                else
                    try
                        R2(i)=fzero(@(x) (Volume(x,Z(i), 0.5*Z(i))-V0(i)),start);
                    catch
                    end
                end
                start=R2(i);
            end
        end
        
        function A=Area(R2, R1, Z)
            A=pi*(4*R1.*(R2 - R1).*(asin(0.5*Z./R1)+cos(asin(0.5*Z./R1))-1)+2*R1.*Z+2*R2.^2-Z.^2/2);
        end
    end

end