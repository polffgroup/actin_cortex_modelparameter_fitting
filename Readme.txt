################################################################################
FITTING OF MODEL PARAMETERS FROM AVERAGED FRAP RECOVERY CURVES AND CELL SQUISHING DATA
################################################################################

This matlab package of scripts determines model parameters by fitting average FRAP recovery curves and squishing data (time evolutions of cortex-to-cytoplasm ratio and cortical tension, averaged)

1) Run the matlab script ‘FRAPandSquishfitting_Publ.m’ 
Input: 
a)	Averaged cortex/cytoplasm ratio with cortical tension over time (output of the script AverageCurves.m" in project https://gitlab.com/polffgroup/actincortexanalysis/)
b)	Averaged FRAP recovery curves (output produced from AnalysisScriptSummary.m in project https://gitlab.com/polffgroup/actincortexanalysis/)

Example file: ‘AveragedRecoveryFRAP_FlnB.fig’ and ‘Tension_CortexFluo_Squish__FlnB.fig’

Output: 
•	text file Output_FRAPSquish_Fitting.txt with fitted parameters
where the first four columns store the binding kinetics parameters: 
kon1 (binding rate of cytoplasmic pool), 
kon2 (binding rate of cortical pool), 
koff (unbinding rate from cortex at zero tension), 
alpha (quantifies mechanosensitivity of unbinding)
•	matlab figure showing FRAP recovery with fit curve (top panel) and squishing cortex-cytoplasm ratio with fit curve (bottom panel) ‘Fitting_FRAP_Squish_ProteinName_RandomString.fig’
Example output: ‘Fitting_FRAP_Squish_FlnB_B_Intweighted.fig’
